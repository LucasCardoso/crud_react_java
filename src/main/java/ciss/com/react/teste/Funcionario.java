package ciss.com.react.teste;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
public class Funcionario {
	
	private @Id @GeneratedValue Long id;
	
	private String nome;
	
	private String sobreNome;
	
	private String email;
	
	private Integer nis;
	
	private @Version @JsonIgnore Long version;
	
	private Funcionario() {}
	
	public Funcionario(String nome, String sobreNome, String email, Integer nis) {
		this.nome = nome;
		this.sobreNome = sobreNome;
		this.email = email;
		this.nis = nis;
	}
	
}
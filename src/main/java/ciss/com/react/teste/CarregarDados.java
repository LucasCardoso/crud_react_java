package ciss.com.react.teste;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CarregarDados implements CommandLineRunner {
	
	private final FuncionarioRepository repository;
	
	@Autowired
	public CarregarDados(FuncionarioRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public void run(String... args) throws Exception {
		this.repository.save(new Funcionario("Lucas", "Cardoso", "lucas@gmail.com", 123456789));
		this.repository.save(new Funcionario("Marcos", "Silva", "marcos@gmail.com", 123456789));
		this.repository.save(new Funcionario("Augusto", "Soares", "augusto@gmail.com", 123456789));
	}

}

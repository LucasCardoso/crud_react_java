package ciss.com.react.teste;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface FuncionarioRepository extends PagingAndSortingRepository<Funcionario, Long>{

}

'use strict';

const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');
const follow = require('./follow');
const root = '/api';
const when = require('when');

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles, makeStyles } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';

const StyledTableCell = withStyles(theme => ({
	  head: {
	    backgroundColor: "#303F9F",
	    color: theme.palette.common.white,
	  },
	  body: {
	    fontSize: 14,
	  },
}))(TableCell);

const useStyles = makeStyles(theme => ({
	  container: {
	    display: 'flex',
	    flexWrap: 'wrap',
	  },
	  textField: {
	    marginLeft: theme.spacing(1),
	    marginRight: theme.spacing(1),
	  },
}));

const onlyNum = new RegExp("^[0-9]*$");
const emailValid = new RegExp("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {funcionarios: [], attributes: [], pageSize: 3, links: {}, erros: {}};
		this.updatePageSize = this.updatePageSize.bind(this);
		this.onCreate = this.onCreate.bind(this);
		this.onUpdate = this.onUpdate.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.onNavigate = this.onNavigate.bind(this);
	}
	
	carregarDados(pageSize) {
		follow(client, root, [
			{rel: 'funcionarios', params: {size: pageSize}}]
		).then(funcionariosLista => {
			return client({
				method: 'GET',
				path: funcionariosLista.entity._links.profile.href,
				headers: {'Accept': 'application/schema+json'}
			}).then(schema => {
				this.schema = schema.entity;
				this.links = funcionariosLista.entity._links;
				return funcionariosLista;
			});
		}).then(funcionariosLista => {
			return funcionariosLista.entity._embedded.funcionarios.map(funcionario =>
				client({
					method: 'GET',
					path: funcionario._links.self.href
				})
			);
		}).then(funcionarioPromises => {
			return when.all(funcionarioPromises);
		}).done(funcionarios => {
			this.setState({
				funcionarios: funcionarios,
				attributes: Object.keys(this.schema.properties),
				pageSize: pageSize,
				links: this.links});
		});
	}

	componentDidMount() {
		this.carregarDados(this.state.pageSize);
	}
	
	onCreate(funcionario) {
		const self = this;
		follow(client, root, ['funcionarios']).then(funcionariosLista => {
			return client({
				method: 'POST',
				path: funcionariosLista.entity._links.self.href,
				entity: funcionario,
				headers: {'Content-Type': 'application/json'}
			})
		}).then(response => {
			return follow(client, root, [
				{rel: 'funcionarios', params: {'size': self.state.pageSize}}]);
		}).done(response => {
			if (typeof response.entity._links.last !== "undefined") {
				this.onNavigate(response.entity._links.last.href);
			} else {
				this.onNavigate(response.entity._links.self.href);
			}
		});
	}
	
	onNavigate(navUri) {
		client({
			method: 'GET',
			path: navUri
		}).then(funcionariosLista => {
			this.links = funcionariosLista.entity._links;

			return funcionariosLista.entity._embedded.funcionarios.map(funcionario =>
					client({
						method: 'GET',
						path: funcionario._links.self.href
					})
			);
		}).then(funcionarioPromises => {
			return when.all(funcionarioPromises);
		}).done(funcionarios => {
			this.setState({
				funcionarios: funcionarios,
				attributes: Object.keys(this.schema.properties),
				pageSize: this.state.pageSize,
				links: this.links
			});
		});
	}
	
	onDelete(funcionario) {
		client({method: 'DELETE', path: funcionario.entity._links.self.href}).done(response => {
			this.carregarDados(this.state.pageSize);
		});
	}
	
	updatePageSize(pageSize) {
		if (pageSize !== this.state.pageSize) {
			this.carregarDados(pageSize);
		}
	}
	
	onUpdate(funcionario, alterarFuncionario) {
		const self = this;
		client({
			method: 'PUT',
			path: funcionario.entity._links.self.href,
			entity: alterarFuncionario,
			headers: {
				'Content-Type': 'application/json',
				'If-Match': funcionario.headers.Etag
			}
		}).done(response => {
			this.carregarDados(self.state.pageSize);
		}, response => {
			if (response.status.code === 412) {
				alert('Não é possível atualizar ' +
						funcionario.entity._links.self.href);
			}
		});
	}
	
	render() {
		return (
			<div>
				<NovoDialog attributes={this.state.attributes} onCreate={this.onCreate}/>
				<FuncionarioList funcionarios={this.state.funcionarios}
							  links={this.state.links}
							  pageSize={this.state.pageSize}
							  onNavigate={this.onNavigate}
							  onDelete={this.onDelete}
							  attributes={this.state.attributes}
				 			  onUpdate={this.onUpdate}
							  updatePageSize={this.updatePageSize}/>
			</div>
		)
	}
}

class FuncionarioList extends React.Component{

	constructor(props) {
		super(props);
		this.handleNavFirst = this.handleNavFirst.bind(this);
		this.handleNavPrev = this.handleNavPrev.bind(this);
		this.handleNavNext = this.handleNavNext.bind(this);
		this.handleNavLast = this.handleNavLast.bind(this);
		this.handleInput = this.handleInput.bind(this);
	}
	
	handleInput(e) {
		e.preventDefault();
		const pageSize = e.target.value;
		if (/^[0-9]+$/.test(pageSize)) {
			this.props.updatePageSize(pageSize);
		}
	}
	
	handleNavFirst(e){
		e.preventDefault();
		this.props.onNavigate(this.props.links.first.href);
	}

	handleNavPrev(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.prev.href);
	}

	handleNavNext(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.next.href);
	}

	handleNavLast(e) {
		e.preventDefault();
		this.props.onNavigate(this.props.links.last.href);
	}
	
	render() {
		const funcionarios = this.props.funcionarios.map((funcionario, index) => 
		<Funcionario key={funcionario.entity._links.self.href}
					 funcionario={funcionario}
					 attributes={this.props.attributes}
					 onUpdate={this.props.onUpdate}
					 onDelete={this.props.onDelete}/>
		);

		const buttons = [];
		if ("first" in this.props.links) {
			buttons.push(<Button variant="contained" color="primary" key="first" onClick={this.handleNavFirst}>&lt;&lt;</Button>);
		}
		if ("prev" in this.props.links) {
			buttons.push(<Button variant="contained" color="primary" key="prev" onClick={this.handleNavPrev}>&lt;</Button>);
		}
		if ("next" in this.props.links) {
			buttons.push(<Button variant="contained" color="primary" key="next" onClick={this.handleNavNext}>&gt;</Button>);
		}
		if ("last" in this.props.links) {
			buttons.push(<Button variant="contained" color="primary" key="last" onClick={this.handleNavLast}>&gt;&gt;</Button>);
		}
		
		return (
			<div>
				<TextField variant="outlined" id="outline-pag" margin="normal" label="Paginação" defaultValue={this.props.pageSize} onChange={this.handleInput} />
				<Table>
					<TableHead>
						<TableRow>
							<StyledTableCell>Nome</StyledTableCell>
							<StyledTableCell>Sobre nome</StyledTableCell>
							<StyledTableCell>E-mail</StyledTableCell>
							<StyledTableCell align="right">NIS</StyledTableCell>
							<StyledTableCell></StyledTableCell>
							<StyledTableCell></StyledTableCell>
						</TableRow>
					</TableHead>
					{funcionarios}
				</Table>
				<div>
					{buttons}
				</div>
			</div>
		)
	}
}

class Funcionario extends React.Component{
	constructor(props) {
		super(props);
		this.handleDelete = this.handleDelete.bind(this);
	}
	
	handleDelete() {
		this.props.onDelete(this.props.funcionario);
	}
	
	render() {
		return (
			<TableBody>
				<TableRow>
					<StyledTableCell>{this.props.funcionario.entity.nome}</StyledTableCell>
					<StyledTableCell>{this.props.funcionario.entity.sobreNome}</StyledTableCell>
					<StyledTableCell>{this.props.funcionario.entity.email}</StyledTableCell>
					<StyledTableCell align="right">{this.props.funcionario.entity.nis}</StyledTableCell>
					<StyledTableCell>
						<AlterarDialog funcionario={this.props.funcionario}
								  	   attributes={this.props.attributes}
								       onUpdate={this.props.onUpdate}/>
					</StyledTableCell>
					<StyledTableCell>
						<IconButton color="secondary"onClick={this.handleDelete}>
							<DeleteIcon />
						</IconButton>
					</StyledTableCell>
				</TableRow>
			</TableBody>
		)
	}
}

class NovoDialog extends React.Component {
	
	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		let invalido = false;
		const funcionario = {};
		this.props.attributes.forEach(attribute => {
			let input = ReactDOM.findDOMNode(this.refs[attribute]);
			let value = input.childNodes[1].childNodes[0].value;
			funcionario[attribute] = value.trim();
			if(attribute == 'nome') {
				if(value.length < 2 ) {
					invalido = true;
					alert("Nome deve ter no mínimo 2 caracteres!");
				} else if (value.length > 30 ){
					invalido = true;
					alert("Nome deve ter no máximo 2 caracteres!");
				} else if (!value){
					invalido = true;
					alert("Nome não pode ficar vazio!");
				}
			} else if (attribute == 'nis') {
				if(!onlyNum.test(value)) {
					invalido = true;
					alert("Nis somente números!");
				} else if (!value){
					invalido = true;
					alert("Nis não pode ficar vazio!");
				}
			} else if (attribute == 'sobreNome') {
				if(value.length < 2 ) {
					invalido = true;
					alert("Sobre nome deve ter no mínimo 2 caracteres!");
				} else if (value.length > 30 ){
					invalido = true;
					alert("Sobre nome deve ter no máximo 50 caracteres!");
				}  else if (!value){
					invalido = true;
					alert("Sobre nome não pode ficar vazio!");
				}
			} else if (attribute == 'email') {
				if(!value) {
					invalido = true;
					alert("E-mail com formato inválido!");
				}  else if (!value){
					invalido = true;
					alert("E-mail não pode ficar vazio!");
				}
			}
		});
		
		if(!invalido) {
			this.props.onCreate(funcionario);
			this.props.attributes.forEach(attribute => {
				let input = ReactDOM.findDOMNode(this.refs[attribute]);
				input.childNodes[1].childNodes[0].value = '';
			});
			window.location = "#";
		}
	}

	render() {
		return (
			<div>
				<Button variant="contained" size="large" color="primary" href="#novoFuncionario">
					Novo
				</Button>
		        <div id="novoFuncionario" className="modalDialog">
					<div>
						<a href="#" title="Fechar" className="fechar">X</a>

						<h2>Novo funcionário</h2>

						<form>
							<div>
								<TextField key="nome" margin="normal" label="Nome" ref="nome" />
								<TextField key="sobreNome" margin="normal" label="Sobre Nome" ref="sobreNome" />
							    <TextField key="email" margin="normal" label="E-mail" ref="email" />
								<TextField key="nis" margin="normal" label="NIS" ref="nis" />
							</div>
							<Button variant="contained" size="large" color="primary" onClick={this.handleSubmit}>Salvar</Button>
						</form>
					</div>
				</div>
			</div>
		)
	}

}

class AlterarDialog extends React.Component {

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e) {
		e.preventDefault();
		let invalido = false;
		const funcionario = {};
		this.props.attributes.forEach(attribute => {
			let input = ReactDOM.findDOMNode(this.refs[attribute]);
			let value = input.childNodes[1].childNodes[0].value;
			funcionario[attribute] = value.trim();
			if(attribute == 'nome') {
				if(value.length < 2 ) {
					invalido = true;
					alert("Nome deve ter no mínimo 2 caracteres!");
				} else if (value.length > 30 ){
					invalido = true;
					alert("Nome deve ter no máximo 2 caracteres!");
				} else if (!value){
					invalido = true;
					alert("Nome não pode ficar vazio!");
				}
			} else if (attribute == 'nis') {
				if(!onlyNum.test(value)) {
					invalido = true;
					alert("Nis somente números!");
				} else if (!value){
					invalido = true;
					alert("Nis não pode ficar vazio!");
				}
			} else if (attribute == 'sobreNome') {
				if(value.length < 2 ) {
					invalido = true;
					alert("Sobre nome deve ter no mínimo 2 caracteres!");
				} else if (value.length > 30 ){
					invalido = true;
					alert("Sobre nome deve ter no máximo 50 caracteres!");
				}  else if (!value){
					invalido = true;
					alert("Sobre nome não pode ficar vazio!");
				}
			} else if (attribute == 'email') {
				if(!value) {
					invalido = true;
					alert("E-mail com formato inválido!");
				}  else if (!value){
					invalido = true;
					alert("E-mail não pode ficar vazio!");
				}
			}
		});
		if(!invalido) {
			this.props.onUpdate(this.props.funcionario, funcionario);
			window.location = "#";
		}
	}

	render() {
		const inputs = this.props.attributes.map(attribute =>
			<p key={this.props.funcionario.entity[attribute]}>
				<input type="text" placeholder={attribute}
					   defaultValue={this.props.funcionario.entity[attribute]}
					   ref={attribute} className="field"/>
			</p>
		);

		const alterarId = "alterarFuncionario-" + this.props.funcionario.entity._links.self.href;
		return (
			<div key={this.props.funcionario.entity._links.self.href}>
				<Button color="primary" href={"#" + alterarId}>
					<EditIcon />
	        	</Button>
				<div id={alterarId} className="modalDialog">
					<div>
						<a href="#" title="Fechar" className="fechar">X</a>

						<h2>Alterar funcionário</h2>

						<form>
							<div>
								<TextField defaultValue={this.props.funcionario.entity['nome']} key="nome" margin="normal" label="Nome" ref="nome" />
								<TextField defaultValue={this.props.funcionario.entity['sobreNome']} key="sobreNome" margin="normal" label="Sobre Nome" ref="sobreNome" />
							    <TextField defaultValue={this.props.funcionario.entity['email']} key="email" margin="normal" label="E-mail" ref="email" />
								<TextField defaultValue={this.props.funcionario.entity['nis']} key="nis" margin="normal" label="NIS" ref="nis" />
							</div>
							<Button variant="contained" size="large" color="primary" onClick={this.handleSubmit}>Alterar</Button>
						</form>
					</div>
				</div>
			</div>
		)
	}

};

ReactDOM.render(
	<App />,
	document.getElementById('react')
)
